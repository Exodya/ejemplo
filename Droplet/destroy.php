<?php 

$data = array("id"=>192693292);
$data_string = json_encode($data);
$ch = curl_init('https://api.digitalocean.com/v2/droplets/192693292');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer ba9f4826cb9a5b224f95edd3eee578e9df8e04763756108e91eba41617408d1a',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);  
$result = curl_exec($ch);
print_r($result);