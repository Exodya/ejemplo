<?php

$personJSON = ' {
    "name": "big cormoration",
    "numberOfEmployees": 1000,
    "ceo": 3.6
}';

$person = json_decode($personJSON);

echo $person->name . "<br>" ;
echo $person->numberOfEmployees . "<br>";
echo $person->ceo . "<br>";
// digital ocean
/** 
$data = array("name" => "daniel-droplet", "region" => "sfo2", "size" => "1gb", "image" => "ubuntu-18-04-x64");
$data_string = json_encode($data);
$ch = curl_init('https://api.digitalocean.com/v2/droplets');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer ba9f4826cb9a5b224f95edd3eee578e9df8e04763756108e91eba41617408d1a',
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);  
$result = curl_exec($ch);
print_r($result);
*/
//{"droplet":{"id":192343374,"name":"daniel-droplet","memory":1024,"vcpus":1,"disk":30,"locked":false,"status":"new","kernel":null,"created_at":"2020-05-15T01:39:42Z","features":[],"backup_ids":[],"next_backup_window":null,"snapshot_ids":[],"image":{"id":53893572,"name":"18.04.3 (LTS) x64","distribution":"Ubuntu","slug":"ubuntu-18-04-x64","public":true,"regions":["nyc3","nyc1","sfo1","nyc2","ams2","sgp1","lon1","nyc3","ams3","fra1","tor1","sfo2","blr1","sfo3"],"created_at":"2019-10-22T01:38:19Z","min_disk_size":20,"type":"base","size_gigabytes":2.36,"description":"Ubuntu 18.04 x64 20191022","tags":[],"status":"available"},"volume_ids":[],"size":{"slug":"1gb","memory":1024,"vcpus":1,"disk":30,"transfer":2.0,"price_monthly":10.0,"price_hourly":0.01488,"regions":["ams2","ams3","blr1","fra1","lon1","nyc1","nyc2","nyc3","sfo1","sfo2","sfo3","sgp1","tor1"],"available":true},"size_slug":"1gb","networks":{"v4":[],"v6":[]},"region":{"name":"San Francisco 2","slug":"sfo2","features":["private_networking","backups","ipv6","metadata","install_agent","storage","image_transfer"],"available":true,"sizes":["s-1vcpu-1gb","512mb","s-1vcpu-2gb","1gb","s-3vcpu-1gb","s-2vcpu-2gb","s-1vcpu-3gb","s-2vcpu-4gb","2gb","s-4vcpu-8gb","m-1vcpu-8gb","c-2","4gb","g-2vcpu-8gb","gd-2vcpu-8gb","m-16gb","s-6vcpu-16gb","c-4","8gb","m-2vcpu-16gb","m3-2vcpu-16gb","g-4vcpu-16gb","gd-4vcpu-16gb","m6-2vcpu-16gb","m-32gb","s-8vcpu-32gb","c-8","16gb","m-4vcpu-32gb","m3-4vcpu-32gb","g-8vcpu-32gb","s-12vcpu-48gb","gd-8vcpu-32gb","m6-4vcpu-32gb","m-64gb","s-16vcpu-64gb","c-16","32gb","m-8vcpu-64gb","m3-8vcpu-64gb","g-16vcpu-64gb","s-20vcpu-96gb","48gb","gd-16vcpu-64gb","m6-8vcpu-64gb","m-128gb","s-24vcpu-128gb","c-32","64gb","m-16vcpu-128gb","m3-16vcpu-128gb","s-32vcpu-192gb","m-24vcpu-192gb","m-224gb","m6-16vcpu-128gb","m3-24vcpu-192gb","m-32vcpu-256gb","m6-24vcpu-192gb","m3-32vcpu-256gb","m6-32vcpu-256gb"]},"tags":[]},"links":{"actions":[{"id":936304987,"rel":"create","href":"https://api.digitalocean.com/v2/actions/936304987"}]}} 1

//{"droplet":{"id":192693292,"name":"daniel-droplet","memory":1024,"vcpus":1,"disk":30,"locked":false,"status":"new","kernel":null,"created_at":"2020-05-18T00:05:53Z","features":[],"backup_ids":[],"next_backup_window":null,"snapshot_ids":[],"image":{"id":53893572,"name":"18.04.3 (LTS) x64","distribution":"Ubuntu","slug":"ubuntu-18-04-x64","public":true,"regions":["nyc3","nyc1","sfo1","nyc2","ams2","sgp1","lon1","nyc3","ams3","fra1","tor1","sfo2","blr1","sfo3"],"created_at":"2019-10-22T01:38:19Z","min_disk_size":20,"type":"base","size_gigabytes":2.36,"description":"Ubuntu 18.04 x64 20191022","tags":[],"status":"available"},"volume_ids":[],"size":{"slug":"1gb","memory":1024,"vcpus":1,"disk":30,"transfer":2.0,"price_monthly":10.0,"price_hourly":0.01488,"regions":["ams2","ams3","blr1","fra1","lon1","nyc1","nyc2","nyc3","sfo1","sfo2","sfo3","sgp1","tor1"],"available":true},"size_slug":"1gb","networks":{"v4":[],"v6":[]},"region":{"name":"San Francisco 2","slug":"sfo2","features":["private_networking","backups","ipv6","metadata","install_agent","storage","image_transfer"],"available":true,"sizes":["s-1vcpu-1gb","512mb","s-1vcpu-2gb","1gb","s-3vcpu-1gb","s-2vcpu-2gb","s-1vcpu-3gb","s-2vcpu-4gb","2gb","s-4vcpu-8gb","m-1vcpu-8gb","c-2","4gb","g-2vcpu-8gb","gd-2vcpu-8gb","m-16gb","s-6vcpu-16gb","c-4","8gb","m-2vcpu-16gb","m3-2vcpu-16gb","g-4vcpu-16gb","gd-4vcpu-16gb","m6-2vcpu-16gb","m-32gb","s-8vcpu-32gb","c-8","16gb","m-4vcpu-32gb","m3-4vcpu-32gb","g-8vcpu-32gb","s-12vcpu-48gb","gd-8vcpu-32gb","m6-4vcpu-32gb","m-64gb","s-16vcpu-64gb","c-16","32gb","m-8vcpu-64gb","m3-8vcpu-64gb","g-16vcpu-64gb","s-20vcpu-96gb","48gb","gd-16vcpu-64gb","m6-8vcpu-64gb","m-128gb","s-24vcpu-128gb","c-32","64gb","m-16vcpu-128gb","m3-16vcpu-128gb","s-32vcpu-192gb","m-24vcpu-192gb","m-224gb","m6-16vcpu-128gb","m3-24vcpu-192gb","m-32vcpu-256gb","m6-24vcpu-192gb","m3-32vcpu-256gb","m6-32vcpu-256gb"]},"tags":[]},"links":{"actions":[{"id":938207823,"rel":"create","href":"https://api.digitalocean.com/v2/actions/938207823"}]}} 1