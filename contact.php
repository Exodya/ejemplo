
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="icon" href="img/car/logo1.svg.png">
    <link href="https://fonts.googleapis.com/css2?family=Liu+Jian+Mao+Cao&display=swap" rel="stylesheet">
    <title>Contact Us</title>
</head>
<body>
    <header class="site-header inicio">      
            <?php
                require('nav/nav.php');
            ?>
                  <div class="nav">
                <nav class="navegacion">
                   <ul>
                   <a href="/"><li>INICIO</li></a>
                       <a href="/store.php"><li>STORE</li></a>
                       <a href="/models.php"><li>MODELS</li></a>
                       <a href="/know.php"><li>KNOW US</li></a>

                   </ul>
                </nav>
            </div>
    </header>
    <section class="section-contact">
        <div class="contact">
        <h2>Contact</h2>
        <div class="nav2">
                <nav class="navegacion2">
                   <ul>
                       <a href="#"><li>About</li></a>
                       <a href="#"><li>Legal</li></a>
                       <a href="#"><li>Investors</li></a>
                       <a href="#"><li>Suplimers</li></a>
                   </ul>
                </nav>
            </div>
        </div>
    
      </section>
    
      <section class="section-contact-list">
          <div class="contact-section">
        <div class="column">
            <address class="vcard">
                                    <h3 class="section-title fn">Sales</h3>
                
                <div class="org">Tesla</div>

                                    <p class="tel">
                        Visit our <a href="/store.php">Find Us</a> page to locate your <br> nearest Tesla store.
                    </p>
                            </address>

            
            <address class="vcard">
                <h3 class="section-title fn" style="line-height:20px; padding-bottom: 10px;">Customer Support &amp; Roadside <br> Assistance</h3>

                <div class="org">Tesla</div>
                                                    <p class="tel" style="margin-bottom:10px;">
                        <span class="type">Visit our <a href="/support">Support</a> page to find answers and <br> learn about our products.</span>
                    </p>
                    <p class="customer_support" style="margin-bottom:10px;">
                        <span class="type">Emergency Roadside Assistance</span>:<br><a href="/roadside-assistance">International phone numbers</a>
                    </p>
                
                                    <p style="margin-bottom:10px;"><a href="/vin-recall-search">Safety recall information</a></p>
                            </address>

        </div>
        <div class="column">
            <div class="careers">
                <h3 class="section-title">Careers</h3>
                <p>Visit our <a href="/careers">careers</a> page for a list of current <br> employment
                    opportunities.</p>
            </div>
                            <div class="first-responders">
                    <h3 class="section-title">First Responders</h3>
                    <p>Visit our <a href="/firstresponders">first responders</a> page to download <br> Tesla
                        reference guides for emergency <br> personnel.</p>
                </div>
                    </div>
        <div class="column last press-contacts">
            <h3 class="section-title press-contacts-title">Press</h3>
            <address class="vcard">
                <div class="region fn">North America</div>
                <div class="email"><a href="mailto:press@tesla.com">Press@tesla.com</a></div>
            </address>
            <address class="vcard">
                <div class="region fn">Europe &amp; Middle East</div>
                <div class="email"><a href="mailto:eupress@tesla.com">EUPress@tesla.com</a></div>
            </address>
            <address class="vcard">
                <div class="region fn">Australia and Asia</div>
                <div class="email"><a href="mailto:apacpress@tesla.com">APACPress@tesla.com</a></div>
            </address>
            <address class="vcard">
                <div class="region fn">China</div>
                <div class="email"><a href="mailto:china-press@tesla.com">China-Press@tesla.com</a></div>
            </address>
        </div>
        </div>
    </section>
  
    <section class="formulario">
        <div class="form">
            <h3>Comentar</h3>
        <form action="fuction/save.php"  method="POST">
            <label for="">Name <br>
                <input name="name" type="text" required>
            </label>
            <br>
            <label for="">Qustion/Comment <br>
            <input name="comment" type="text" class="comment" required >
            </label> <br>
            <input type="submit" name="salve" value="Post" class="buttom" required>	
        </form>
        </div>
    </section>

    <section class="comentaris">
    <?php
    require('database.php') ;

    ?>
    <div class="comenta">
    <h2>Comentarios</h2>
    <div class="coment">
    <?php  foreach ($tasks as $task) : ?>
       
            <h3><?= htmlspecialchars($task->name) ?></h3>
        
        <div class="cometa2">
            <?= htmlspecialchars($task->comment) ?>
        
                <form action="fuction/delete.php" method="POST">
                <?=   '<button type="submit" class="delete" name="id" value="'.$task->id.'">Delete</button> '?>
                </form>
                </div>
        <?php   endforeach; ?>
    </div>

</div>
    </section>
    <section>
    <footer class="site-footer seccion">     
            <p class="copyrigth">Todos los derechos reservados 2020&copy;</p>
    </footer>
    </section>
</body>
</html>
